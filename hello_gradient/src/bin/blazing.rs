#![no_std]
#![no_main]

use core::iter::repeat;
use lights::{
    gamma::GammaCorrector,
    PixelIterator,
    rgb::{
        gray
    }
};
use lights_hal::{boot, delay, entry};

#[entry]
fn main() -> ! {
    let mut lights = GammaCorrector(boot());

    let mut pattern = repeat(gray(0)).take(15).chain(repeat(gray(255)).take(15)).cycle();

    loop {
        pattern.clone().take(125).render_to(&mut lights);

        pattern.next();
        delay(1_000_000);
    }
}

#![no_std]
#![no_main]

use core::iter::repeat;
use house::Harrogate;
use lights::rgb::{linear_gradient, Rgb};
use lights_hal::{boot, delay, entry};

const OFF: Rgb = Rgb(0, 0, 0);
const ON: Rgb = Rgb(255, 255, 255);

/*
 * 20 fade + 25 ON + 20 fade = 65 dots
 * 85 OFF + 65 door = 150 dots
 */

#[entry]
fn main() -> ! {
    let mut lights = boot();
    lights.heartbeat();

    loop {
        // blink light to demonstrate loop is still running
        lights.heartbeat();

        let door_highlight = linear_gradient(OFF, ON, 20)
            .chain(repeat(ON).take(25))
            .chain(linear_gradient(ON, OFF, 20));

        Harrogate {
            porch_back: repeat(OFF).take(85).chain(door_highlight),
            porch_front: repeat(OFF),
        }.render_to(&mut lights);

        delay(48_000_000);
    }
}

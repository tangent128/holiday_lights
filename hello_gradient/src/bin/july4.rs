#![no_std]
#![no_main]

use core::iter::repeat;
use lights::{
    gamma::correct,
    HardwareRgb,
    PixelIterator,
    rgb::{
        Rgb
    }
};
use lights_hal::{boot, delay, entry};

#[entry]
fn main() -> ! {
    let mut lights = boot();
    lights.heartbeat();

    let mut pattern =
        repeat(&Rgb(255,0,0)).take(5)
        .chain(
            repeat(&Rgb(255,255,255)).take(5)
        ).cycle().take(60)
        .chain(
            repeat(&Rgb(255,0,0)).take(5)
        )
        .chain(
            [
                Rgb(0,0,255),
                Rgb(255,255,255),
                Rgb(0,0,255),
            ].iter().cycle().take(85)
        ).cycle();

    let mut buffer = [HardwareRgb(255,255,0); 200];
    //delay(48_000_000 * 3);
    loop {
        lights.heartbeat();

        for (i, pixel) in pattern.clone().take(200).enumerate() {
            buffer[i] = correct(pixel);
            //buffer[i] = HardwareRgb(255,128,0);
        }

        buffer.iter().render_to(&mut lights);
        // buffer.iter().render_to(&mut lights);
        // lights.out.set_high();
        // lights.high_out.set_low();
        // lights.byte(0);
        // lights.byte(0);
        // lights.byte(255);

        pattern.next();
        delay(1_000_000);
    }
}

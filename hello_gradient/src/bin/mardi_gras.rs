#![no_std]
#![no_main]

use core::iter::repeat;
use house::Harrogate;
use lights::rgb::{linear_gradient, Rgb};
use lights_hal::{boot, delay, entry};

const OFF: Rgb = Rgb(0, 0, 0);
const YELLOW: Rgb = Rgb(255, 255, 0);
const GREEN: Rgb = Rgb(100, 255, 0);
const PURPLE: Rgb = Rgb(196, 0, 255);

const SEGMENT_LEN: usize = 50;

#[entry]
fn main() -> ! {
    let mut lights = boot();
    lights.heartbeat();

    let mut gradient = linear_gradient(YELLOW, GREEN, SEGMENT_LEN)
        .chain(linear_gradient(GREEN, PURPLE, SEGMENT_LEN))
        .chain(linear_gradient(PURPLE, YELLOW, SEGMENT_LEN))
        .cycle();

    loop {
        // blink light to demonstrate loop is still running
        lights.heartbeat();

        Harrogate {
            porch_back: gradient.clone(),
            porch_front: repeat(OFF),
        }.render_to(&mut lights);

        gradient.next();
        delay(500_000);
    }
}

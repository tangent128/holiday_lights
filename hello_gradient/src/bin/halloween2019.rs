#![no_std]
#![no_main]

use core::iter::repeat;
use lights::{
    gamma::correct,
    HardwareRgb,
    PixelIterator,
    rgb::{
        Rgb
    }
};
use lights_hal::{boot, delay, entry};

const ORANGE: Rgb = Rgb(255,150,0);
const PURPLE: Rgb = Rgb(200,0,255);

const BUFFER_LEN: usize = 300;

#[entry]
fn main() -> ! {
    let mut lights = boot();
    lights.heartbeat();

    let front_pattern = [
        PURPLE,
        PURPLE,
        ORANGE,
        PURPLE,
        PURPLE,
    ].iter().cycle();
    let back_pattern = repeat(&ORANGE);

    let pattern =
        back_pattern.take(150)
        .chain(
            front_pattern.take(150)
        ).cycle();

    let mut buffer = [HardwareRgb(255,255,0); BUFFER_LEN];
    loop {
        // blink light to demonstrate loop is still running
        lights.heartbeat();

        for (i, pixel) in pattern.clone().take(BUFFER_LEN).enumerate() {
            buffer[i] = correct(pixel);
        }

        buffer.iter().render_to(&mut lights);

        delay(12_000_000);
    }
}

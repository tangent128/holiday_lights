#!/bin/sh

set -e

APPLET=$1
TARGET_BIN="target/thumbv6m-none-eabi/release/$APPLET"
test -e "$TARGET_BIN" || {
    echo "'$APPLET' does not exist"
    exit 1
}

arm-none-eabi-objcopy -O binary "$TARGET_BIN" flash.bin
uf2conv-rs flash.bin -o flash.uf2

sudo mount LABEL=ITSYBOOT mnt/
echo "Flashing..."
sudo cp flash.uf2 mnt/flash.uf2
sync
echo "Done."
sudo umount mnt/

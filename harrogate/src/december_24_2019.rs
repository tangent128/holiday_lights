//! ramp gradient of red and green
use super::delay;
use house::Harrogate;
use lights::rgb::{blend_steps, Rgb};
use lights::{HardwareRgb, Lights};

const OFF: Rgb = Rgb(0, 0, 0);
const GREEN: Rgb = Rgb(0, 200, 0);
const RED: Rgb = Rgb(200, 0, 0);

const SEGMENT_LEN: usize = 35;
const ZOOM: usize = 10;
const ZOOM_SEGMENT_LEN: usize = SEGMENT_LEN * ZOOM;
const OVERLAP: usize = 60;
const RAMP_LEN: usize = ZOOM_SEGMENT_LEN / 4 + OVERLAP;

const HALF_PORCH: usize = 75;
const FULL_PORCH: usize = 150;

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let mut t = 0;

    loop {
        // advance "time" for pattern
        t += 1;
        if t > ZOOM_SEGMENT_LEN {
            t = 0;
        }

        let ramp_func = |x, center, color| {
            if x < center {
                if x < (center - RAMP_LEN) {
                    OFF
                } else {
                    blend_steps(OFF, color, RAMP_LEN, x - (center - RAMP_LEN))
                }
            } else {
                if x > (center + RAMP_LEN) {
                    OFF
                } else {
                    blend_steps(color, OFF, RAMP_LEN, x - center)
                }
            }
        };

        let half_pattern_func = |i| {
            let x = (i * ZOOM) + t;
            let x = x % ZOOM_SEGMENT_LEN;
            ramp_func(x, 0, RED)
                + ramp_func(x, ZOOM_SEGMENT_LEN, RED)
                + ramp_func(x, ZOOM_SEGMENT_LEN / 2, GREEN)
        };
        let pattern_func = |i| match i {
            i if i < HALF_PORCH => half_pattern_func(i),
            i => half_pattern_func(FULL_PORCH - i),
        };

        Harrogate {
            porch_back: (0..FULL_PORCH).map(|_| pattern_func(HALF_PORCH)),
            porch_front: (0..FULL_PORCH).map(pattern_func),
        }
        .render_to(lights);

        delay(300_000);
    }
}

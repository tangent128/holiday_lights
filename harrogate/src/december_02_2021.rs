use super::delay;
use core::iter::repeat;
use house::Harrogate;
use lights::{HardwareRgb, Lights, murmurf};
use lights::rgb::Rgb;

const YELLOW: Rgb = Rgb(200, 200, 0);
const RED: Rgb = Rgb(200, 0, 0);
const GREEN: Rgb = Rgb(0, 200, 0);
const BLUE: Rgb = Rgb(0, 0, 200);

/*
 * 20 fade + 25 ON + 20 fade = 65 dots
 * 85 OFF + 65 door = 150 dots
 */

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let mut rng: u32 = 0xF07;

    loop {

        let mut colors = [RED, YELLOW, GREEN, BLUE, RED, YELLOW, GREEN, BLUE];

        // shuffle
        for i in 2usize..=8 {
            let swap_with = murmurf(&mut rng) as usize % i;
            (colors[i - 1], colors[swap_with]) = (colors[swap_with], colors[i - 1]);
        }

        Harrogate {
            porch_back: colors.into_iter().flat_map(|color| repeat(color).take(19)),
            porch_front: colors.into_iter().flat_map(|color| repeat(color).take(19)),
        }.render_to(lights);

        delay(48_000_000 / 3);
    }
}

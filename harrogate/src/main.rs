#![no_std]
#![no_main]

use lights_hal::{boot, entry};

pub use lights_hal::delay;

mod december_01_2019;
mod december_21_2019;
mod december_24_2019;
mod december_02_2020;
mod december_02_2021;
mod valentines_2020;
mod st_patricks_day;
mod july_4;
mod halloween_2020;
mod halloween_2021;
mod halloween_2022;
mod halloween_2023;
mod halloween_2024;
mod november_2020;
mod new_years_eve_2020;
mod door_light;
mod door_light_orange;

#[entry]
fn main() -> ! {
    let mut lights = boot();

    #[cfg(feature = "december-01-2019")]
    december_01_2019::run(&mut lights);

    #[cfg(feature = "december-21-2019")]
    december_21_2019::run(&mut lights);

    #[cfg(feature = "december-24-2019")]
    december_24_2019::run(&mut lights);

    #[cfg(feature = "december-02-2020")]
    december_02_2020::run(&mut lights);

    #[cfg(feature = "december-02-2021")]
    december_02_2021::run(&mut lights);

    #[cfg(feature = "valentines-2020")]
    valentines_2020::run(&mut lights);

    #[cfg(feature = "st-patricks-day")]
    st_patricks_day::run(&mut lights);

    #[cfg(feature = "july-4")]
    july_4::run(&mut lights);

    #[cfg(feature = "halloween-2020")]
    halloween_2020::run(&mut lights);

    #[cfg(feature = "halloween-2021")]
    halloween_2021::run(&mut lights);

    #[cfg(feature = "halloween-2022")]
    halloween_2022::run(&mut lights);

    #[cfg(feature = "halloween-2023")]
    halloween_2023::run(&mut lights);

    #[cfg(feature = "halloween-2024")]
    halloween_2024::run(&mut lights);

    #[cfg(feature = "new-years-eve-2020")]
    new_years_eve_2020::run(&mut lights);

    #[cfg(feature = "november-2020")]
    november_2020::run(&mut lights);

    #[cfg(not(feature = "replace-default"))]
    door_light::run(&mut lights);

    #[cfg(feature = "door-light-orange")]
    door_light_orange::run(&mut lights);
}

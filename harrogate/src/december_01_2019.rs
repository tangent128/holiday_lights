//! Streamers of white on a shifting color backdrop
use super::delay;
use house::Harrogate;
use lights::rgb::{blend, blend_steps, linear_gradient, Rgb};
use lights::{HardwareRgb, Lights};

const OFF: Rgb = Rgb(0, 0, 0);
const GREEN: Rgb = Rgb(0, 200, 0);
const BLUE: Rgb = Rgb(0, 0, 200);
const PURPLE: Rgb = Rgb(160, 0, 128);
const WHITE: Rgb = Rgb(255, 255, 255);

const PULSE_LEN: usize = 200;
const SEGMENT_LEN: usize = 35;
const ZOOM: usize = 10;
const ZOOM_SEGMENT_LEN: usize = SEGMENT_LEN * ZOOM + ZOOM;

const HALF_PORCH: usize = 75;
const FULL_PORCH: usize = 150;

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let mut color_pulse = linear_gradient(GREEN, BLUE, PULSE_LEN)
        .chain(linear_gradient(BLUE, PURPLE, PULSE_LEN))
        .chain(linear_gradient(PURPLE, GREEN, PULSE_LEN))
        .cycle();

    let mut t = 0;

    loop {
        // advance "time" for pattern
        t += 1;
        if t > ZOOM_SEGMENT_LEN {
            t = 0;
        }
        let color = color_pulse.next().unwrap_or(Rgb(255, 0, 0));

        let half_pattern_func = |i| {
            let x = (i * ZOOM) + t;
            let x = x % ZOOM_SEGMENT_LEN;
            match x {
                x if x > SEGMENT_LEN * ZOOM => {
                    blend_steps(OFF, WHITE, ZOOM, x - SEGMENT_LEN * ZOOM)
                }
                x => blend_steps(WHITE, OFF, SEGMENT_LEN * ZOOM, x),
            }
        };
        let pattern_func = |i| match i {
            i if i < HALF_PORCH => half_pattern_func(i),
            i => half_pattern_func(FULL_PORCH - i),
        };

        Harrogate {
            porch_back: (0..FULL_PORCH).map(|_| color),
            porch_front: (0..FULL_PORCH)
                .map(pattern_func)
                .map(|b| blend(color, WHITE, b)),
        }
        .render_to(lights);

        delay(300_000);
    }
}

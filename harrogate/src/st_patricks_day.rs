use super::delay;
use core::iter::repeat;
use house::Harrogate;
use lights::rgb::Rgb;
use lights::{HardwareRgb, Lights};

const GREEN: Rgb = Rgb(0, 255, 0);

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let pattern = Harrogate {
        porch_back: repeat(GREEN),
        porch_front: repeat(GREEN),
    };

    loop {
        pattern.clone().render_to(lights);

        delay(48_000_000);
    }
}

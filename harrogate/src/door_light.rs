use super::delay;
use core::iter::repeat;
use house::Harrogate;
use lights::{HardwareRgb, Lights};
use lights::rgb::{linear_gradient, Rgb};

const OFF: Rgb = Rgb(0, 0, 0);
const ON: Rgb = Rgb(255, 255, 255);

/*
 * 20 fade + 25 ON + 20 fade = 65 dots
 * 85 OFF + 65 door = 150 dots
 */

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let door_highlight = linear_gradient(OFF, ON, 20)
    .chain(repeat(ON).take(25))
    .chain(linear_gradient(ON, OFF, 20));
    
    let pattern = Harrogate {
        porch_back: repeat(OFF).take(85).chain(door_highlight),
        porch_front: repeat(OFF),
    };
    
    loop {
        pattern.clone().render_to(lights);

        delay(48_000_000);
    }
}

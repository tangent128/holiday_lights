//! purple-and-green with "lightning" flashes
use core::iter::repeat;

use super::delay;
use house::{Harrogate, PORCH_BACK_LEN, PORCH_FRONT_LEN};
use lights::{rgb::Rgb, HardwareRgb, Lights};

const ORANGE: Rgb = Rgb(255, 150, 0);
const PURPLE: Rgb = Rgb(100, 0, 128);

// tenth of a second
const TIC: u32 = 4_800_000;

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let mut ring = repeat(PURPLE)
        .take(PORCH_BACK_LEN)
        .chain(repeat(ORANGE).take(PORCH_FRONT_LEN))
        .cycle();

    loop {
        let back = ring.clone();
        let front = back.clone().skip(PORCH_BACK_LEN);

        // render
        Harrogate {
            porch_back: back,
            porch_front: front,
        }
        .render_to(lights);

        ring.next();

        delay(TIC * 2);
    }
}

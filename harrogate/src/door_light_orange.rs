use super::delay;
use core::iter::repeat;
use house::Harrogate;
use lights::rgb::{linear_gradient, Rgb};
use lights::{HardwareRgb, Lights};

const OFF: Rgb = Rgb(0, 0, 0);
const ON: Rgb = Rgb(255, 192, 128);

/*
 * 20 fade + 25 ON + 20 fade = 65 dots
 * 85 OFF + 65 door = 150 dots
 */

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let mut i = 0;
    loop {
        i = match i {
            i if i < 84 => i + 1,
            _ => 0,
        };

        let void = (0..).map(|j| if i == j { OFF } else { OFF });

        let door_highlight = linear_gradient(OFF, ON, 20)
            .chain(repeat(ON).take(25))
            .chain(linear_gradient(ON, OFF, 20));

        let pattern = Harrogate {
            porch_back: void.take(85).chain(door_highlight),
            porch_front: repeat(OFF),
        };

        pattern.render_to(lights);

        delay(48_000_000);
        //lights.latch();
    }
}

//! purple-and-orange with chaotically-varying background

use super::delay;
use house::{Harrogate, PORCH_BACK_LEN, PORCH_FRONT_LEN};
use lights::{murmurf, rgb::Rgb, HardwareRgb, Lights};

const BLACK: Rgb = Rgb(0, 0, 0);
const ORANGE: Rgb = Rgb(255, 150, 0);
const PURPLE: Rgb = Rgb(100, 0, 128);

const COLORS: &[Rgb] = &[
    Rgb(255, 0, 0),
    Rgb(0, 1, 0),
    Rgb(150, 150, 150),
    Rgb(0, 1, 0),
    Rgb(255, 150, 0),
    Rgb(0, 1, 0),
    Rgb(150, 0, 255),
    Rgb(0, 1, 0),
];

/*const RAINBOW: &[Rgb] = &[
    Rgb(255, 0, 0),
    Rgb(255, 150, 0),
    Rgb(255, 255, 0),
    Rgb(150, 255, 0),
    Rgb(0, 255, 0),
    Rgb(0, 255, 255),
    Rgb(0, 0, 255),
    Rgb(150, 0, 255),
    Rgb(255, 0, 150),
];*/

// a hundredth of a second
const TIC: u32 = 48_000_0;

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let mut rng = 0x642u32;

    let mut bar = 0;
    let mut color_index: usize = 0;

    let mut back_buffer = [BLACK; PORCH_BACK_LEN];

    let mut draw = |back_buffer: &[Rgb]| {
        let front_pattern = [PURPLE, ORANGE].iter().cycle().cloned();
        Harrogate {
            porch_front: front_pattern.take(PORCH_FRONT_LEN),
            porch_back: back_buffer.iter().cloned(),
        }
        .render_to(lights);
    };

    loop {
        // animate rainbow bars
        if bar == 0 {
            color_index = (color_index + 1 + (murmurf(&mut rng) as usize % 6)) % COLORS.len();
            bar = 10;
        }
        bar -= 1;

        // push into buffer
        back_buffer.rotate_right(1);
        back_buffer[0] = COLORS[color_index];
        back_buffer[PORCH_BACK_LEN - 1].1 = 255;

        // render
        draw(&back_buffer);

        delay(TIC);
    }
}

use super::delay;
use core::iter::repeat;
use house::Harrogate;
use lights::rgb::Rgb;
use lights::{HardwareRgb, Lights};

const WHITE: Rgb = Rgb(255, 255, 255);
const YELLOW: Rgb = Rgb(255, 255, 0);

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let pattern = Harrogate {
        porch_back: repeat(YELLOW),
        porch_front: repeat(WHITE),
    };

    loop {
        pattern.clone().render_to(lights);

        delay(48_000_000);
    }
}

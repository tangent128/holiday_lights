use super::delay;
use core::iter::repeat;
use house::Harrogate;
use lights::rgb::Rgb;
use lights::{HardwareRgb, Lights};

const YELLOW: Rgb = Rgb(255, 195, 31);
const ORANGE: Rgb = Rgb(220, 117, 0);
const PUMPKIN: Rgb = Rgb(172, 67, 12);
//const BROWN: Rgb = Rgb(118, 41, 0);
const BROWN: Rgb = Rgb(90, 34, 0);
const RED: Rgb = Rgb(130, 18, 16);
const OFF: Rgb = Rgb(0, 0, 0);

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let pattern = Harrogate {
        porch_back: repeat(OFF)
            .take(87)
            .chain(repeat(RED).take(6))
            .chain(repeat(BROWN).take(6))
            .chain(repeat(PUMPKIN).take(6))
            .chain(repeat(ORANGE).take(6))
            .chain(repeat(YELLOW).take(12))
            .chain(repeat(ORANGE).take(6))
            .chain(repeat(PUMPKIN).take(6))
            .chain(repeat(BROWN).take(6))
            .chain(repeat(RED).take(6))
            .chain(repeat(OFF).take(3)),
        porch_front: repeat(OFF),
    };

    loop {
        pattern.clone().render_to(lights);

        delay(48_000_000);
    }
}

//! purple-and-orange with chaotically-varying background

use super::delay;
use house::{Harrogate, PORCH_BACK_LEN, PORCH_FRONT_LEN};
use lights::{
    murmurf,
    rgb::{gray, Rgb},
    HardwareRgb, Lights,
};

const BLACK: Rgb = Rgb(0, 0, 0);
const BG_WAVE_1: Rgb = Rgb(200, 100, 0);
const BG_WAVE_2: Rgb = Rgb(0, 100, 100);
const ORANGE: Rgb = Rgb(255, 150, 0);
const PURPLE: Rgb = Rgb(100, 0, 128);

// tenth of a second
const TIC: u32 = 4_800_000;

enum Direction {
    UP,
    DOWN,
}

struct Wave {
    spot: u8,
    target: u8,
    direction: Direction,
}

impl Wave {
    fn tick(&mut self, rng: &mut u32) {
        if self.spot == self.target {
            // target caught, pick new one
            self.target = (murmurf(rng) % 256) as u8;
            self.direction = if self.target > self.spot {
                Direction::UP
            } else {
                Direction::DOWN
            };
        } else {
            // chase target brightness
            self.spot = match self.direction {
                Direction::UP => self.spot + 1,
                Direction::DOWN => self.spot - 1,
            }
        }
    }

    fn rgb(&self, color: Rgb) -> Rgb {
        color * gray(self.spot)
    }
}

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let mut rng = 0xB00u32;
    let mut wave1 = Wave {
        spot: 128,
        target: 128,
        direction: Direction::UP,
    };
    let mut wave2 = Wave {
        spot: 128,
        target: 128,
        direction: Direction::UP,
    };

    let mut back_buffer = [BLACK; PORCH_BACK_LEN];

    let mut draw = |back_buffer: &[Rgb]| {
        let front_pattern = [PURPLE, ORANGE].iter().cycle().cloned();
        Harrogate {
            porch_front: front_pattern.take(PORCH_FRONT_LEN),
            porch_back: back_buffer.iter().cloned(),
        }
        .render_to(lights);
    };

    loop {
        // animate brightness variation
        wave1.tick(&mut rng);
        wave2.tick(&mut rng);

        // push into buffer
        back_buffer.rotate_right(1);
        back_buffer[0] = wave1.rgb(BG_WAVE_1) + wave2.rgb(BG_WAVE_2);
        back_buffer[PORCH_BACK_LEN - 1].1 = 255;

        // render
        draw(&back_buffer);

        delay(TIC);
    }
}

//! Golden background with twinkling lights
use super::delay;
use core::convert::TryInto;
use house::Harrogate;
use lights::rgb::Rgb;
use lights::{HardwareRgb, Lights, murmurf};

const GOLD: Rgb = Rgb(200, 200, 0);
const RED: Rgb = Rgb(0, 200, 0);
const GREEN: Rgb = Rgb(0, 200, 0);
const BLUE: Rgb = Rgb(0, 0, 200);
const PURPLE: Rgb = Rgb(160, 0, 128);
const WHITE: Rgb = Rgb(255, 255, 255);

const FULL_PORCH: usize = 150;

#[derive(Copy, Clone)]
struct Pixel {
    color: Rgb,
    delay: u16
}

impl Pixel {
    const fn new() -> Pixel {
        Pixel {
            color: GOLD,
            delay: 0,
        }
    }

    fn tick(&mut self, rng: &mut u32) {
        if self.delay > 0 {
            self.delay -= 1;
        } else {
            self.color = match murmurf(rng) % 16 {
                0..=1 => RED,
                2..=3 => GREEN,
                4..=5 => BLUE,
                6 => PURPLE,
                7 => WHITE,
                _ => GOLD,
            };

            self.delay = if self.color == GOLD {
                (murmurf(rng) % 600 + 300).try_into().unwrap_or(1000)
            } else {
                (murmurf(rng) % 100 + 50).try_into().unwrap_or(0)
            }
        }
    }
}

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {

    let mut rng = 1;
    let mut pixels = [Pixel::new(); FULL_PORCH];

    loop {
        for pixel in pixels.iter_mut() {
            pixel.tick(&mut rng);
        }

        Harrogate {
            porch_back: (0..FULL_PORCH).map(|_| GOLD),
            porch_front: pixels.iter().map(|p| p.color)
        }
        .render_to(lights);

        delay(5_000_000 / 60);
    }
}

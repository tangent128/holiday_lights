//! 50-star USA Flag, stars on front, stripes on back.
use super::delay;
use core::iter::repeat;
use house::Harrogate;
use lights::rgb::Rgb;
use lights::{HardwareRgb, Lights};

const OFF: Rgb = Rgb(0, 0, 0);
const RED: Rgb = Rgb(255, 0, 0);
const WHITE: Rgb = Rgb(255, 255, 255);
const BLUE: Rgb = Rgb(0, 0, 200);

const FULL_PORCH: usize = 150;

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let mut heartbeat = false;
    loop {
        heartbeat = !heartbeat;

        let stars = [BLUE, WHITE, BLUE].iter().cycle().take(FULL_PORCH).cloned();
        let stripes = repeat(RED)
            .take(11)
            .chain(repeat(WHITE).take(11))
            .cycle()
            .take(11 * 12)
            .chain(repeat(RED).take(11))
            .chain(repeat(OFF).take(6))
            .chain(repeat(if heartbeat { RED } else { OFF }))
            .take(FULL_PORCH);

        let pattern = Harrogate {
            porch_back: stripes,
            porch_front: stars,
        };

        pattern.render_to(lights);

        delay(24_000_000);
    }
}

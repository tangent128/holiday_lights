//! purple-and-green with "lightning" flashes
use super::delay;
use house::{Harrogate, PORCH_BACK_LEN, PORCH_FRONT_LEN};
use lights::{murmurf, rgb::Rgb, HardwareRgb, Lights};

const BLACK: Rgb = Rgb(0, 0, 0);
const GREEN: Rgb = Rgb(0, 255, 0);
const LIGHTNING: Rgb = Rgb(255, 200, 50);

const PURPLE: Rgb = Rgb(100, 0, 128);

// tenth of a second
const TIC: u32 = 4_800_000;

#[allow(dead_code)]
#[inline(always)]
pub fn run(lights: &mut impl Lights<Pixel = HardwareRgb>) -> ! {
    let mut rng = 0xB00u32;
    let mut back_buffer = [BLACK; PORCH_BACK_LEN];

    let mut draw = |back_buffer: &[Rgb]| {
        let front_pattern = [PURPLE].iter().cycle().cloned();
        Harrogate {
            porch_front: front_pattern.take(PORCH_FRONT_LEN),
            porch_back: back_buffer.iter().cloned(),
        }
        .render_to(lights);
    };

    loop {
        // clear
        back_buffer.iter_mut().for_each(|pix| *pix = GREEN);
        draw(&back_buffer);

        // delay 3-15 seconds
        delay(TIC * (murmurf(&mut rng) % 120 + 30));

        // flash 1
        back_buffer.iter_mut().for_each(|pix| *pix = BLACK);
        flash(&mut rng, &mut back_buffer);
        draw(&back_buffer);
        delay(TIC * 2);

        // flash 2
        flash(&mut rng, &mut back_buffer);
        draw(&back_buffer);
        delay(TIC * 3);
    }
}

fn flash(rng: &mut u32, back_buffer: &mut [Rgb]) {
    let start = murmurf(rng) as usize % (PORCH_BACK_LEN - 20);
    let len = murmurf(rng) as usize % 20 + murmurf(rng) as usize % 20 + 10;
    let end = (start + len).min(PORCH_BACK_LEN);
    back_buffer.as_mut()[start..end]
        .iter_mut()
        .for_each(|pix| *pix = LIGHTNING);
}

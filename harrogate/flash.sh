#!/bin/sh

set -e

cd "$(dirname "$0")"

cargo build --release --features $1

APPLET=harrogate
TARGET_BIN="target/thumbv6m-none-eabi/release/$APPLET"
test -e "$TARGET_BIN" || {
    echo "'$APPLET' does not exist"
    exit 1
}

arm-none-eabi-objcopy -O binary "$TARGET_BIN" image.bin
uf2conv-rs image.bin -o image.uf2

sudo mount LABEL=ITSYBOOT mnt/
echo "Flashing..."
sudo cp image.uf2 mnt/image.uf2
sync
echo "Done."
sudo umount mnt/

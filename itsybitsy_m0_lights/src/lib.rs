#![no_std]

use itsybitsy_m0::hal::gpio::v2::PushPullOutput;
use panic_halt as _;

pub use cortex_m::asm::delay;

use core::arch::asm;
use core::sync::atomic::{compiler_fence, Ordering};
use core::u8;
use hal::prelude::*;
pub use itsybitsy_m0::entry;
use itsybitsy_m0::hal as hal;
use lights::{HardwareRgb, Lights};

pub fn boot() -> NeopixelLights {
    let mut peripherals = hal::pac::Peripherals::take().unwrap();
    let _clock = hal::clock::GenericClockController::with_internal_32kosc(
        peripherals.GCLK,
        &mut peripherals.PM,
        &mut peripherals.SYSCTRL,
        &mut peripherals.NVMCTRL,
    );

    let pins = itsybitsy_m0::Pins::new(peripherals.PORT);

    NeopixelLights {
        high_out: pins.d5.into(),
        red_led: pins.d13.into(),
        debug_light: false,
    }
}

// WS2815 ideal lighting times
// approx. 20ns per clock cycle;
// const ZERO_HIGH_CYCLES: u32 = 11; // about 220ns
// const ONE_HIGH_CYCLES: u32 = 29; // about 580ns
// const ZERO_LOW_CYCLES: u32 = 29; // about 580ns
// const ONE_LOW_CYCLES: u32 = 11; // about 220ns

const LATCH_CYCLES: u32 = 15000; // about 300us

pub struct NeopixelLights {
    pub high_out: hal::gpio::v2::Pin<hal::gpio::v2::PA15, PushPullOutput>,
    pub red_led: itsybitsy_m0::RedLed,
    pub debug_light: bool,
}

impl NeopixelLights {
    #[inline]
    pub fn write(&mut self, bit: bool) {
        if bit {
            unsafe {
                compiler_fence(Ordering::SeqCst);
                self.high_out.set_high().unwrap();
                compiler_fence(Ordering::SeqCst);
                asm!(
                    // 15 nops
                    "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;",
                    "nop;", "nop;", "nop;", "nop;", "nop;",
                );
                compiler_fence(Ordering::SeqCst);
                self.high_out.set_low().unwrap();
                compiler_fence(Ordering::SeqCst);
                asm!(
                    // 8 nops
                    "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;",
                );
                compiler_fence(Ordering::SeqCst);
            }
        } else {
            unsafe {
                compiler_fence(Ordering::SeqCst);
                self.high_out.set_high().unwrap();
                compiler_fence(Ordering::SeqCst);
                asm!(
                    // 8 nops
                    "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;",
                );
                compiler_fence(Ordering::SeqCst);
                self.high_out.set_low().unwrap();
                compiler_fence(Ordering::SeqCst);
                asm!(
                    // 15 nops
                    "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;",
                    "nop;", "nop;", "nop;", "nop;", "nop;",
                );
                compiler_fence(Ordering::SeqCst);
            }
        }

        if false {
            // go high
            self.high_out.set_high().unwrap();

            // experimentally, there is some unknown overhead
            // but these timings appear to work for me
            unsafe {
                compiler_fence(Ordering::SeqCst);
                asm!(
                    // 8 nops
                    "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;",
                );
                compiler_fence(Ordering::SeqCst);
            }

            if bit {
                unsafe {
                    compiler_fence(Ordering::SeqCst);
                    asm!(
                        // 14 nops
                        "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;",
                        "nop;", "nop;", "nop;", "nop;", "nop;",
                    );
                    compiler_fence(Ordering::SeqCst);
                }
            }

            // go low
            self.high_out.set_low().unwrap();

            unsafe {
                compiler_fence(Ordering::SeqCst);
                asm!(
                    // 15 nops
                    "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;", "nop;",
                    "nop;", "nop;", "nop;", "nop;", "nop;",
                );
                compiler_fence(Ordering::SeqCst);
            }
        }
    }

    #[inline]
    pub fn byte(&mut self, byte: u8) {
        self.write(byte & 0x80 != 0);
        self.write(byte & 0x40 != 0);
        self.write(byte & 0x20 != 0);
        self.write(byte & 0x10 != 0);
        self.write(byte & 0x08 != 0);
        self.write(byte & 0x04 != 0);
        self.write(byte & 0x02 != 0);
        self.write(byte & 0x01 != 0);
        unsafe {
            compiler_fence(Ordering::SeqCst);
            asm!(
                // 5 nops
                "nop;", "nop;", "nop;", "nop;", "nop;",
            );
            compiler_fence(Ordering::SeqCst);
        }
    }

    /// Blink light, probably to indicate a loop is still running
    pub fn heartbeat(&mut self) {
        if self.debug_light {
            self.red_led.set_low().unwrap();
        } else {
            self.red_led.set_high().unwrap();
        }
        self.debug_light = !self.debug_light;
    }
}

impl Lights for NeopixelLights {
    type Pixel = HardwareRgb;

    #[inline]
    fn render(&mut self, rgb: &HardwareRgb) {
        // GRB pixel order
        self.byte(rgb.1);
        self.byte(rgb.0);
        self.byte(rgb.2);
    }
    #[inline]
    fn latch(&mut self) {
        delay(LATCH_CYCLES);
    }
}

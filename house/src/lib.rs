#![no_std]

use lights::HardwareRgb;
use lights::gamma::correct;
use lights::Lights;
use lights::PixelIterator;
use lights::rgb::Rgb;

pub const PORCH_BACK_LEN: usize = 150;
pub const PORCH_FRONT_LEN: usize = 150;

const PORCH_WHITE: Rgb = Rgb(255, 208, 160);

#[derive(Copy, Clone)]
pub struct Harrogate<PB, PF> {
    pub porch_back: PB,
    pub porch_front: PF,
}

#[inline]
fn white_balance(HardwareRgb(r, g, b): HardwareRgb, white: Rgb) -> HardwareRgb {
    let Rgb(r, g, b) = Rgb(r, g, b) * white;
    HardwareRgb(r, g, b)
}

impl<PB, PF> Harrogate<PB, PF>
where
    PB: IntoIterator<Item = Rgb>,
    PF: IntoIterator<Item = Rgb>,
{
    #[inline]
    pub fn render_to<L: Lights<Pixel = HardwareRgb>>(self, lights: &mut L) {
        let mut buffer = [HardwareRgb(255,0,255); PORCH_BACK_LEN + PORCH_FRONT_LEN];

        for (i, pixel) in self.porch_back.into_iter().take(PORCH_BACK_LEN).enumerate() {
            buffer[PORCH_BACK_LEN - i] = white_balance(correct(&pixel), PORCH_WHITE);
        }
        for (i, pixel) in self.porch_front.into_iter().take(PORCH_FRONT_LEN).enumerate() {
            buffer[PORCH_BACK_LEN + i] = white_balance(correct(&pixel), PORCH_WHITE);
        }

        buffer.iter().render_to(lights);
    }
}

#[cfg(test)]
mod tests {
    // #[test]
}
